# SecureNLP

The repository consists of source codes used for the algorithms used for our securenlp work at https://competitions.codalab.org/competitions/17262

# Files
The repository consists of two files namely model.py and crf.prop

# Description
#### model.py 
This file contains the model definition of convolutional neural network used for subtask 1. The model usage and build instructions are similar to https://github.com/richliao/textClassifier. Plug in the model defintion inside https://github.com/richliao/textClassifier/blob/master/textClassifierConv.py
#### crf.pop 
this file contains the properties file that can be used to train CRF for subtask 2. The training instructions are explained in https://nlp.stanford.edu/software/crf-faq.html#a